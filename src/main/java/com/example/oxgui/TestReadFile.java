package com.example.oxgui;


import com.example.oxgui.friend;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author wittaya
 */
public class TestReadFile {
    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
        File file = new File("friends.dat");
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);
        friend f1 = (friend) ois.readObject();
        friend f2 = (friend) ois.readObject();
        System.out.println(f1);
        System.out.println(f2);
        ois.close();
        fis.close();
    }  
}
